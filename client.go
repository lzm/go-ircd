package main

import (
	"bufio"
	"log"
	"net"
	"strings"
)

type Client struct {
	addr     string
	nick     string
	output   chan string
	server   *Server
	conn     net.Conn
	quitting bool
}

func NewClient(conn net.Conn, server *Server) *Client {
	c := &Client{
		addr:   conn.RemoteAddr().String(),
		conn:   conn,
		server: server,
		output: make(chan string),
	}

	go c.readLoop()
	go c.writeLoop()

	return c
}

func (c *Client) readLoop() {
	reader := bufio.NewReader(c.conn)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			log.Println("c.reader", c.addr, err)
			break
		}

		c.server.input <- NewCommand(c, line)
	}

	c.beginQuit()
}

func (c *Client) writeLoop() {
	writer := bufio.NewWriter(c.conn)

	for {
		str, ok := <-c.output
		if !ok {
			log.Println("c.output", c.addr, ok)
			break
		}

		_, err := writer.WriteString(str)
		if err != nil {
			log.Println("c.writer", c.addr, err)
			break
		}

		if strings.Contains(str, "\n") {
			writer.Flush()
		}
	}

	c.beginQuit()
}

func (c *Client) beginQuit() {
	if !c.quitting {
		c.quitting = true
		c.server.input <- NewCommand(c, "QUIT")
	}
}

func (c *Client) Quit() {
	c.quitting = true
	c.conn.Close()
	close(c.output)
}
