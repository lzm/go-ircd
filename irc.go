package main

type Handler func(srv *Server, cl *Client, args string)

func HandleQuit(srv *Server, client *Client, args string) {
	delete(srv.clients, client.addr)
	client.Quit()
}

func HandleNick(srv *Server, client *Client, args string) {
	nick, _ := Split2(args)

	if client.nick == "" {
		client.nick = nick
		client.output <- ":srv 001 " + client.nick + " :welcome!\n"
	}
}

func HandleJoin(srv *Server, client *Client, args string) {

}

func HandlePrivmsg(srv *Server, client *Client, args string) {
	target, msg := Split2(args)

	for _, c := range srv.clients {
		c.output <- client.addr + " -> " + target + ": " + msg + "\n"
	}
}

func HandlePing(srv *Server, client *Client, args string) {
	client.output <- ":srv PONG " + args + "\n"
}

func HandleUser(srv *Server, client *Client, args string) {

}

func HandleMode(srv *Server, client *Client, args string) {

}
