package main

import (
	"strings"
)

func SplitN(line string, n int) []string {
	ret := make([]string, n)
	splitted := strings.SplitN(line, " ", n)
	for i, v := range splitted {
		ret[i] = v
	}
	return ret
}

func Split2(line string) (string, string) {
	ret := SplitN(line, 2)
	return ret[0], ret[1]
}
