package main

import (
	"log"
	"net"
)

type Server struct {
	input    chan *Command
	clients  map[string]*Client
	handlers map[string]Handler
	name     string
}

func NewServer() *Server {
	srv := &Server{
		name:    "lzmserv",
		input:   make(chan *Command),
		clients: make(map[string]*Client),
		handlers: map[string]Handler{
			"NICK":    HandleNick,
			"USER":    HandleUser,
			"MODE":    HandleMode,
			"QUIT":    HandleQuit,
			"JOIN":    HandleJoin,
			"PRIVMSG": HandlePrivmsg,
			"PING":    HandlePing,
		},
	}

	go srv.processLoop()

	return srv
}

func (srv *Server) loop(address string) {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalln("listen error: ", err)
	}
	log.Println("listening on ", listener.Addr())

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println("accept error: ", err)
			continue
		}
		log.Println("new client:", conn.RemoteAddr())

		client := NewClient(conn, srv)
		srv.clients[client.addr] = client
	}
}

func (srv *Server) processLoop() {
	for cmd := range srv.input {
		log.Println(cmd.client.addr, cmd.command, cmd.args)

		handler, ok := srv.handlers[cmd.command]
		if ok {
			handler(srv, cmd.client, cmd.args)
		} else {
			cmd.client.output <- "Unknown command " + cmd.command + "\n"
		}
	}
}
