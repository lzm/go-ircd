package main

import (
	"strings"
)

type Command struct {
	client  *Client
	command string
	args    string
}

func NewCommand(client *Client, line string) *Command {
	trimmed := strings.TrimSpace(line)
	command, args := Split2(trimmed)

	return &Command{
		client:  client,
		command: strings.ToUpper(command),
		args:    args,
	}
}
